import logo from './logo.svg';
import './App.css';
import React, { useState, useEffect } from 'react';
import axios from 'axios';

function App() {
  const [data, setData] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

	useEffect(() => { 

  axios.post('http://54.255.240.119:8080/students',{
  course: "Experiments, Science, and Fashion in Nanophotonics",
  email: "jdoe@example.com",
  gpa: 3,
  name: "Jane Doe"
})

	/*
    axios({
    method: 'post',
    url: 'http://54.255.240.119:8080/students',
    //API要求的資料
    data: {
  course: "Experiments, Science, and Fashion in Nanophotonics",
  email: "jdoe@example.com",
  gpa: 3,
  name: "Jane Doe"
}
})
*/
      .then((response) => {
        console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
		

		axios.get('http://54.255.240.119:8080/students/')
      .then((response) => {
        setData(response.data);
        setLoading(false);
      })
      .catch((error) => {
        setError(error);
        setLoading(false);
      });
      
  }, []);

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error: {error.message}</p>;


	return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
		<div>
      <h1>{data.title}</h1>
      <p>{data.body}</p>
    </div>
    </div>
  );
}

export default App;
